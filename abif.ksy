meta:
  id: abif
  file-extension: ab1
  endian: be
seq:
  - id: magic
    type: str
    size: 4
    encoding: ascii
  - id: header
    type: header

enums:
  element_type:
    1: byte # Unsigned 8-bit integer
    2: char # 8-bit ASCII character or signed 8-bit integer
    3: word # Unsigned 16-bit integer
    4: short # Signed 16-bit integer
    5: long # Signed 32-bit integer
    7: float # 32-bit floating point value
    8: double # 64-bit floating point value
    10: date # YY-M-D
    11: time # hmss
    18: p_string # pascal string
    19: c_string # null terminated string

    # supported legacy types
    12: thumbprint
    13: bool # One-byte boolean value
    1024: user_type # 1024+ are user data types

    # unsupported legacy types
    6: rational
    9: binary_coded_decimal
    14: point
    15: rect
    16: v_point
    17: v_rect
    20: tag
    128: delta_compressed
    256: lzw_compressed
    384: delta_lzw

types:
  header:
    seq:
      - id: version
        type: u2
      - id: directory
        type: directory_entry
      - id: reserved
        size: 94

  directory_entry:
    seq:
      - id: name
        type: str
        size: 4
        encoding: ascii
      - id: number
        type: s4
      - id: element_type
        type: s2
        enum: element_type
      - id: element_size
        type: s2
      - id: number_of_elements
        type: s4
      - id: data_size
        type: s4
      - id: data_offset
        type: s4
      - id: data_handle
        type: s4

instances:
  directory:
    pos: header.directory.data_offset
    id: entry
    type: directory_entry
    repeat: expr
    repeat-expr: header.directory.number_of_elements
